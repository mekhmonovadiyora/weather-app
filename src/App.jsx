import Container from "./components/Container";

export default function App() {
  return (
    <div className="wrapper">
      <Container />
    </div>
  );
}
