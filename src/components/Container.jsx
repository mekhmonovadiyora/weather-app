import { useEffect, useState } from "react";

import CurrentWeather from "./CurrentWeather";
import RightBar from "./RightBar";
import { setWeather } from "../redux/weatherSlice";
import { useDispatch } from "react-redux";

const apiKey = import.meta.env.VITE_API_KEY;

export default function Container() {
  const [weatherData, setWeatherData] = useState({});
  const [cityId, setCityId] = useState("1484842");
  const dispatch = useDispatch();
  async function getWeather() {
    const response = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${apiKey}`
    );
    const data = await response.json();
    dispatch(setWeather(data));
    setWeatherData(data);
  }
  useEffect(() => {
    getWeather();
  }, [cityId]);
  return (
    <div className="container">
      <CurrentWeather weatherData={weatherData} />
      <RightBar weatherData={weatherData} setCityId={setCityId} />
    </div>
  );
}
