import cities from "../services/cities.js";
import { useState } from "react";

export default function RightBar({ weatherData, setCityId }) {
  const [search, setSearch] = useState();
  return (
    <div className="right-bar">
      <div className="search">
        <input
          value={search || ""}
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          placeholder="Another location"
          style={{
            border: "none",
            outline: "none",
            width: "80%",
            fontSize: "1.2rem",
            backgroundColor: "transparent",
            color: "white",
            borderBottom: "1px solid white",
            padding: "0 0 10px 0",
            placeholder: "white",
          }}
        />

        <div
          className="search_button"
          onClick={() => {
            let city;
            {
              cities?.forEach((c) => {
                if (c?.name.toLowerCase() == search.toLowerCase()) {
                  city = c?.id;
                }
              });
            }
            if (!city) {
              console.log("found");
              alert("City not found");
            } else {
              console.log("not found");
              setCityId(city);
            }
          }}
        >
          <img
            src="https://img.icons8.com/ios/50/000/search--v1.png"
            alt="search"
          />
        </div>
      </div>
      <div className="city_list">
        {cities
          ?.filter((city) => city.index < 4 && city?.name !== weatherData?.name)
          ?.map((city, index) => (
            <div key={index}>
              <p
                style={{ cursor: "pointer" }}
                onClick={() => setCityId(city?.id)}
                className="font"
              >
                {city?.name}
              </p>
            </div>
          ))}
      </div>
      <div className="weather_detail">
        <h4>Weather Details</h4>
        <div className="weather_detail_list">
          <div className="weather_detail_item">
            <p className="font" style={{ opacity: 0.7 }}>
              Cloudy
            </p>
            <p>{weatherData?.clouds?.all}%</p>
          </div>
          <div className="weather_detail_item">
            <p className="font" style={{ opacity: 0.7 }}>
              Humidity
            </p>
            <p>{weatherData?.main?.humidity}%</p>
          </div>
          <div className="weather_detail_item">
            <p className="font" style={{ opacity: 0.7 }}>
              Wind
            </p>
            <p>{weatherData?.wind?.speed}km/h</p>
          </div>
          <div className="weather_detail_item">
            <p className="font" style={{ opacity: 0.7 }}>
              Rain
            </p>
            <p>0mm</p>
          </div>
        </div>
      </div>
    </div>
  );
}
