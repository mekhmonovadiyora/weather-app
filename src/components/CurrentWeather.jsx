export default function CurrentWeather({ weatherData }) {
  return (
    <div
      className="main"
      style={{
        backgroundImage: `url("https://source.unsplash.com/1600x900/?${weatherData?.weather?.[0]?.main}")`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="logo">the.weather</div>
      <div className="info">
        <h1 className="title">
          {Math.trunc((Number(weatherData?.main?.temp) - 32) / 1.8) || "0"}°
        </h1>
        <div className="city">
          <h1 style={{ marginBottom: "10px" }}>{weatherData?.name}</h1>
          <div className="city_date">
            <p>
              {new Date().toLocaleTimeString("uz-UZ", {
                hour: "numeric",
                minute: "numeric",
              })}
            </p>
            <span style={{ marginLeft: "5px", marginRight: "5px" }}>-</span>
            <p>
              {new Date().toLocaleDateString("en-US", {
                weekday: "long",
                month: "long",
                day: "numeric",
              })}
            </p>
          </div>
        </div>
        <div className="city_cloud">
          <img
            src={`http://openweathermap.org/img/wn/${weatherData?.weather?.[0]?.icon}.png`}
            alt="weather"
          />
          <p style={{ fontWeight: 500 }}>{weatherData?.weather?.[0]?.main}</p>
        </div>
      </div>
    </div>
  );
}
