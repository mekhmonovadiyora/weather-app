export default [
  { name: "Karakalpakstan", id: "453752", index: 0 },
  { name: "Tashkent", id: "1512569", index: 1 },
  { name: "Sirdaryo", id: "1512770", index: 2 },
  { name: "Jizzakh Province", id: "1484844", index: 3 },
  { name: "Samarqand Viloyati", id: "1114927", index: 4 },
  { name: "Bukhara", id: "1114929", index: 5 },
  { name: "Qashqadaryo", id: "1114928", index: 6 },
  { name: "Surxondaryo Viloyati", id: "1114926", index: 7 },
  { name: "Xorazm Viloyati", id: "1484843", index: 8 },
  { name: "Andijan", id: "1514588", index: 9 },
  { name: "Fergana", id: "1514019", index: 10 },
  { name: "Namangan", id: "1484842", index: 11 },
];
